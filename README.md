# Indirect Active Learning

This repository contains a Python implementation of simulations supporting the paper:

  Singh, S. (2022). *Indirect Active Learning*.

A preprint of the paper is available on [ArXiv](https://arxiv.org/abs/2206.01454).

To reproduce Figures 3, 4, and 5 of that paper:
1) Install [Python 3.9](https://www.python.org/downloads/release/python-390/).
2) Run `pip install -r requirements.txt`. You may want to do this within a [virtual environment](https://docs.python.org/3/tutorial/venv.html).
3) To reproduce Figure 3, run `python figure_3.py`. This took us ~30 minutes to run.
To reproduce Figure 4, run `python figure_4.py`. This took us ~4 hours to run.
To reproduce Figure 5, run `python figure_5.py`. This took us ~1 second to run.

We ran these scripts on an Ubuntu laptop with Intel Core i7-10750H CPU.
