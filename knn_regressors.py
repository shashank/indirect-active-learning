import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.neighbors import KNeighborsRegressor

import matplotlib.pyplot as plt

def _CV_knn_regressor(n, ks=None, cv=4):
  """Returns a knn regressor that chooses k from ks by cv-fold cross-validation."""

  if not ks:
    ks = np.logspace(0, int(np.log2(n)-2), int(np.log2(n)-1), base=2., dtype=int)
    ks = [k for k in ks if k <= 100]
    if ks == []:
      ks = [1]

  regressor = KNeighborsRegressor()
  param_grid = {'n_neighbors': ks}
  knn_gscv = GridSearchCV(regressor, param_grid, cv=cv)
  return knn_gscv


def passive_kernel_regressor(Xs, Ys, Zs, x_0, method='theory', alpha=1.0):

  regressor = GaussianProcessRegressor(alpha=alpha)
  regressor.fit(Xs, Ys)
  return regressor.predict(x_0)

def passive_knn_regressor(Xs, Ys, Zs, x_0, method='theory', sigma_Y=1.0):
  """Regresses passive samples of Y over X."""

  if method == 'theory':
    n, d_X = Xs.shape
    k = max(1, min(n, int(((sigma_Y)**(2*d_X/(2 + d_X)))*n**(2/(2 + d_X)))))
    regressor = KNeighborsRegressor(n_neighbors=k)

  elif method == 'cv':
    regressor =_CV_knn_regressor(len(Ys))

  else:
    raise ValueError(f'method was {method} but should be \'theory\' or \'cv\'.')

  regressor.fit(Xs, Ys)
  return regressor.predict(x_0)


def active_knn_regressor(sampler, x_0, n, method='theory', sigma_X=1.0, sigma_Y=1.0):
  """Regresses Y over X using active learning to select informative Xs."""

  m = int(n/2)

  # Purely exploratory sampling
  Xs1, Ys1, Zs1 = sampler.generate_joint_samples(m)
  if method == 'theory':
    d_Z = Zs1.shape[1]
    k_g = max(1, min(m, int(((sigma_X)**(2*d_Z/(2 + d_Z)))*m**(2/(2 + d_Z)))))
    g_regressor = KNeighborsRegressor(n_neighbors=k_g)

  elif method == 'cv':
    g_regressor =_CV_knn_regressor(m)

  else:
    raise ValueError(f'method was {method} but should be \'theory\' or \'cv\'.')

  g_regressor.fit(Zs1, Xs1)
  # Select best value of Z from exploratory sample
  errors = ((x_0 - g_regressor.predict(Zs1))**2).sum(axis=1)
  best_z = Zs1[np.argmin(errors), :]

  # Purely exploitative sampling
  Xs2, Ys2, Zs2 = sampler.generate_conditional_samples(best_z, n - m)

  Xs = np.concatenate((Xs1, Xs2))
  Ys = np.concatenate((Ys1, Ys2))
  Zs = np.concatenate((Zs1, Zs2))

  if method == 'theory':
    d_X = Xs.shape[1]
    k_f = max(1, min(n, int(((sigma_Y)**(2*d_X/(2 + d_X)))*n**(2/(2 + d_X)))))
    f_regressor = KNeighborsRegressor(n_neighbors=k_f)
  elif method == 'cv':
    f_regressor =_CV_knn_regressor(len(Ys))

  f_regressor.fit(Xs, Ys)
  return f_regressor.predict(x_0), Xs, Ys, Zs


def oracle_knn_regressor(sampler, x_0, z_0, n, method='theory', sigma_Y=1.0):

  Xs, Ys, Zs = sampler.generate_conditional_samples(z_0, n)

  return passive_knn_regressor(Xs, Ys, Zs, x_0, method=method, sigma_Y=sigma_Y)
