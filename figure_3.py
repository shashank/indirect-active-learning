import math
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from knn_regressors import passive_knn_regressor, active_knn_regressor, oracle_knn_regressor
from sampler import Sampler

# Prevent matplotlib from using Type 3 fonts
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

plt.rc('font', size=20)  # Increase matplotlib default font size
np.random.seed(0)  # Fix random seed for reproducibility

num_replicates = 1024  # Number of IID replications of experiment

def _compare_models(ns, s_gs, d_Zs, d_Xs, sigma_Xs, sigma_Ys, f):

  passive_errors = np.zeros((len(ns), len(s_gs), len(d_Zs), len(d_Xs), len(sigma_Xs), len(sigma_Ys), num_replicates))
  passive_cv_errors = np.zeros((len(ns), len(s_gs), len(d_Zs), len(d_Xs), len(sigma_Xs), len(sigma_Ys), num_replicates))
  active_errors = np.zeros((len(ns), len(s_gs), len(d_Zs), len(d_Xs), len(sigma_Xs), len(sigma_Ys), num_replicates))
  active_cv_errors = np.zeros((len(ns), len(s_gs), len(d_Zs), len(d_Xs), len(sigma_Xs), len(sigma_Ys), num_replicates))
  oracle_errors = np.zeros((len(ns), len(s_gs), len(d_Zs), len(d_Xs), len(sigma_Xs), len(sigma_Ys), num_replicates))
  oracle_cv_errors = np.zeros((len(ns), len(s_gs), len(d_Zs), len(d_Xs), len(sigma_Xs), len(sigma_Ys), num_replicates))
  
  for replicate in tqdm(range(num_replicates)):
    for d_Z_idx, d_Z in enumerate(d_Zs):
      for d_X_idx, d_X in enumerate(d_Xs):
        for sigma_X_idx, sigma_X in enumerate(sigma_Xs):
          for sigma_Y_idx, sigma_Y in enumerate(sigma_Ys):
            for s_g_idx, s_g in enumerate(s_gs):
              sampler = Sampler(d_Z, d_X, sigma_X, sigma_Y, s_g, f)
              x_0 = sampler.x_0
              f_x_0 = sampler.f(x_0)
              for n_idx, n in enumerate(ns):
  
                Xs, Ys, Zs = sampler.generate_joint_samples(n)
                passive_estimate = passive_knn_regressor(Xs, Ys, Zs, x_0, sigma_Y=sigma_Y)
                passive_errors[n_idx, s_g_idx, d_Z_idx, d_X_idx, sigma_X_idx, sigma_Y_idx, replicate] = passive_estimate - f_x_0

                passive_cv_estimate = passive_knn_regressor(Xs, Ys, Zs, x_0, 'cv')
                passive_cv_errors[n_idx, s_g_idx, d_Z_idx, d_X_idx, sigma_X_idx,sigma_Y_idx,  replicate] = passive_cv_estimate - f_x_0
  
                active_estimate, _, _, _ = active_knn_regressor(sampler, x_0, n, sigma_X=sigma_X, sigma_Y=sigma_Y)
                active_errors[n_idx, s_g_idx, d_Z_idx, d_X_idx, sigma_X_idx, sigma_Y_idx, replicate] = active_estimate - f_x_0

                active_cv_estimate, _, _, _ = active_knn_regressor(sampler, x_0, n, 'cv')
                active_cv_errors[n_idx, s_g_idx, d_Z_idx, d_X_idx, sigma_X_idx, sigma_Y_idx, replicate] = active_cv_estimate - f_x_0
  
                oracle_estimate = oracle_knn_regressor(sampler, x_0, sampler.z_0, n, sigma_Y=sigma_Y)
                oracle_errors[n_idx, s_g_idx, d_Z_idx, d_X_idx, sigma_X_idx, sigma_Y_idx, replicate] = oracle_estimate - f_x_0

                oracle_cv_estimate = oracle_knn_regressor(sampler, x_0, sampler.z_0, n, method='cv')
                oracle_cv_errors[n_idx, s_g_idx, d_Z_idx, d_X_idx, sigma_X_idx, sigma_Y_idx, replicate] = oracle_cv_estimate - f_x_0

  return passive_errors, passive_cv_errors, active_cv_errors, active_errors, oracle_errors, oracle_cv_errors

def _plot_all(xs, ns=[2**12], s_gs=[1.0], d_Zs=[3], d_Xs=[3], sigma_Xs=[1.0], sigma_Ys=[1.0], f=None):

  passive_errors, passive_cv_errors, active_cv_errors, active_errors, oracle_errors, oracle_cv_errors = (x.squeeze() for x in _compare_models(ns, s_gs, d_Zs, d_Xs, sigma_Xs, sigma_Ys, f))
  def _mse(errors):
    return (errors**2).mean(axis=1)
  def _ste(errors):
    return (errors**2).std(axis=1)/(num_replicates**(1/2))
  plt.errorbar(xs, _mse(passive_errors),    _ste(passive_errors),             marker='o', markersize=10, lw=2, label='Passive')
  plt.errorbar(xs, _mse(passive_cv_errors), _ste(passive_cv_errors), ls='--', marker='o', markersize=10, lw=2, label='Passive CV')
  plt.errorbar(xs, _mse(active_errors),     _ste(active_errors),              marker='^', markersize=10, lw=2, label='Active')
  plt.errorbar(xs, _mse(active_cv_errors),  _ste(active_cv_errors),  ls='--', marker='^', markersize=10, lw=2, label='Active CV')
  plt.errorbar(xs, _mse(oracle_errors),     _ste(oracle_errors),              marker='s', markersize=10, lw=2, label='Oracle')
  plt.errorbar(xs, _mse(oracle_cv_errors),  _ste(oracle_cv_errors),  ls='--', marker='s', markersize=10, lw=2, label='Oracle CV')

print('Generating Panel (a): performance over sample size n...')
plt.subplot(2, 3, 1)
ns = np.logspace(4, 12, 9, base=2., dtype=int)
_plot_all(ns=ns, xs=ns)
plt.gca().set_xscale('log', base=2.0)
plt.gca().set_yscale('log')
plt.xlabel('Sample size ($n$)')
plt.ylabel('Mean Squared Error')
plt.legend()

print('Generating Panel (b): performance over intrinsic dimention d_Z...')
plt.subplot(2, 3, 2)
d_Zs = range(1, 11)
_plot_all(d_Zs=d_Zs, xs=d_Zs)
plt.gca().set_yscale('log')
plt.xlabel('Intrinsic Dimension ($d_Z$)')

print('Generating Panel (c): performance over covariate noise level sigma_X...')
plt.subplot(2, 3, 3)
sigma_Xs = np.logspace(-1, 1, 10)
_plot_all(sigma_Xs=sigma_Xs, xs=sigma_Xs)
plt.loglog()
plt.xlabel('Covariate Noise Level ($\sigma_X$)')

print('Generating Panel (d): performance over smoothness s_g of g...')
plt.subplot(2, 3, 4)
s_gs = np.linspace(0.1, 1.0, 10)
_plot_all(s_gs=s_gs, xs=s_gs)
plt.gca().set_yscale('log')
plt.xlabel('Smoothness ($s_g$) of $g$')
plt.ylabel('Mean Squared Error')

print('Generating Panel (e): performance over covariate noise dimention d_X...')
plt.subplot(2, 3, 5)
d_Xs = range(1, 11)
_plot_all(d_Xs=d_Xs, xs=d_Xs)
plt.gca().set_yscale('log')
plt.xlabel('Covariate Noise Dimension ($d_X$)')

print('Generating Panel (f): performance over response noise level sigma_Y...')
plt.subplot(2, 3, 6)
sigma_Ys = np.logspace(-1, 1, 10)
_plot_all(sigma_Ys=sigma_Ys, xs=sigma_Ys)
plt.loglog()
plt.xlabel('Response Noise Level ($\sigma_Y$)')

plt.show()
