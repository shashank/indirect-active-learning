import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import beta, dirichlet

def _sim(T=100, N=1000, S0=None, I0=10, R0=0, D0=0, beta=0.2, gamma=0.1, delta=0.01):
  """Simulates T steps of a stochastic SIR model.
    T: (int>=0) Number timesteps to compute forward
    N: (int>=0) Total population
    S0: (int>=0) Initial number of susceptible individuals
    I0: (int>=0) Initial number of infected individuals
    R0: (int>=0) Initial number of recovered individuals
    D0: (int>=0) Initial number of dead individuals
    beta: ([0, 1]-valued) Transmission rate
    gamma: ([0, 1]-valued) Recovery rate
    delta: ([0, 1]-valued) Death rate
  """

  if not S0:
    S0 = N - I0 - R0 - D0

  S, I, R, D = S0, I0, R0, D0
  Ss = [S0]
  Is = [I0]
  Rs = [R0]
  Ds = [D0]

  for t in range(T):
    
    dSdt = -np.random.binomial(S, beta*I/N)
    S += dSdt
    R, D, I = np.random.multinomial(I, [gamma, delta, 1 - (gamma + delta)])
    I -= dSdt
    Ss.append(S)
    Is.append(I)
    Rs.append(Rs[-1] + R)
    Ds.append(Ds[-1] + D)

  return np.array(Ss), np.array(Is), np.array(Rs), np.array(Ds)

class Sampler:

  def __init__(self, a=2, b=8, alpha=np.array([1.0, 0.3, 6.7]), T=100, sim_len=20):
    self.beta = beta(a, b)
    self.dirichlet = dirichlet(alpha)
    self.T = T
    self.sim_len = sim_len
    self.d_X = 4*sim_len

  def _get_conditional_samples(self, Zs, n):
    Xs = np.zeros((n, self.d_X))
    Ys = np.zeros((n, 1))
  
    for i in range(n):
      Ss, Is, Rs, Ds = _sim(beta=Zs[i, 0], gamma=Zs[i, 1], delta=Zs[i, 2], T=self.T)
      Xs[i, :] = np.concatenate([Ss[:self.sim_len],
                                 Is[:self.sim_len],
                                 Rs[:self.sim_len],
                                 Ds[:self.sim_len]])
      Ys[i, :] = Ds[-1]
    return Xs, Ys, Zs

  def generate_conditional_samples(self, z, n):
    """Generates n independent samples of (X, Y, Z) conditioned on Z = z."""

    Zs = np.tile(z, [n, 1])
    return self._get_conditional_samples(Zs, n)
  
  def generate_joint_samples(self, n):
    """Generates n independent samples of (X, Y, Z)."""

    Zs = np.concatenate([self.beta.rvs(size=(n,1)), self.dirichlet.rvs(size=n)], axis=1)
    return self._get_conditional_samples(Zs, n)


if __name__ == "__main__":
  ts = np.linspace(0, 150, 150)
  b = beta.rvs(a=2, b=5)
  d = dirichlet.rvs([0.9, 0.1, 9.0])
  gamma, delta = d[0, 0], d[0, 1]

  Ss, Is, Rs, Ds = _sim(len(ts)-1, beta=b, gamma=gamma, delta=delta)
  
  # Plot the data on three separate curves for S(t), I(t) and R(t)
  fig = plt.figure(facecolor='w')
  ax = fig.add_subplot(111, facecolor='#dddddd', axisbelow=True)
  ax.plot(ts, Ss/N, alpha=0.5, lw=2, label='Susceptible')
  ax.plot(ts, Is/N, alpha=0.5, lw=2, label='Infected')
  ax.plot(ts, Rs/N, alpha=0.5, lw=2, label='Recovered with immunity')
  ax.plot(ts, Ds/N, alpha=0.5, lw=2, label='Dead')
  ax.set_xlabel('Time /days')
  ax.set_ylabel('Number (1000s)')
  ax.grid(visible=True, which='major', c='w', lw=2, ls='-')
  legend = ax.legend()
  legend.get_frame().set_alpha(0.5)
  for spine in ('top', 'right', 'bottom', 'left'):
      ax.spines[spine].set_visible(False)
  print(f'Total deaths: {Ds[-1]}')
  plt.show()
