import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from knn_regressors import passive_knn_regressor, active_knn_regressor
from SIR_simulation import Sampler

# Prevent matplotlib from using Type 3 fonts
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42

plt.rc('font', size=22)  # Increase matplotlib default font size
np.random.seed(0)  # Fix random seed for reproducibility

num_replicates = 2**10 # Number of IID replications of experiment
ns = np.logspace(4, 12, 9, base=2., dtype=int)

sim_len = 20
sampler = Sampler(sim_len=sim_len)

passive_knn_errors = np.zeros((num_replicates, len(ns)))
passive_errors = np.zeros((num_replicates, len(ns)))
active_errors = np.zeros((num_replicates, len(ns)))
for replicate in tqdm(range(num_replicates)):
  for n_idx, n in enumerate(ns):
    # Generate some hypothetical "ground truth" from the simulator
    Xs_ground_truth, Ys_ground_truth, Zs_ground_truth = sampler.generate_joint_samples(1)

    # Simulated data for passive estimators
    Xs, Ys, Zs = sampler.generate_joint_samples(n)
    passive_errors[replicate, n_idx] = passive_knn_regressor(Xs, Ys, Zs, Xs_ground_truth, method='cv') - Ys_ground_truth
    
    active_estimates, Xs_active, Ys_active, Zs_active = active_knn_regressor(sampler, Xs_ground_truth, n, method='cv')
    active_errors[replicate, n_idx] = active_estimates - Ys_ground_truth

def _mae(errors):
  return np.abs(errors).mean(axis=0)
def _ste(errors):
  return np.abs(errors).std(axis=0)/(num_replicates**(1/2))

plt.figure(figsize=(12, 12))
plt.errorbar(ns, _mae(passive_errors), _ste(passive_errors), ls='--', c='orange', marker='o', markersize=10, lw=2, label='Passive CV')
plt.errorbar(ns, _mae(active_errors), _ste(active_errors), ls='--', c='r', marker='^', markersize=10, lw=2, label='Active CV')
plt.legend()
plt.gca().set_xscale('log', base=2.0)
plt.gca().set_yscale('log')
plt.grid(visible=True)
plt.ylabel('Mean Absolute Error (MAE)')
plt.xlabel('Sample Size ($n$)')
plt.show()
