import numpy as np

class Sampler:

  def __init__(self, d_Z, d_X, sigma_X, sigma_Y, s_g, f):

    self.d_Z = d_Z
    self.sigma_X = sigma_X
    self.sigma_Y = sigma_Y
    self.g = lambda z: np.tile(4*(np.abs(z)**(s_g)).sum(axis=1), [d_X, 1]).transpose()
    self.f = lambda x: (x**2).sum(axis=1)
    self.z_0 = np.zeros((1, d_Z))
    self.x_0 = np.zeros((1, d_X))
  
  def generate_conditional_samples(self, z, n):
    """Generates n independent samples of (X, Y) conditioned on Z = z."""

    Zs = np.tile(z, [n, 1])

    Xs = self.g(Zs)
    Xs = Xs + np.random.normal(0, self.sigma_X, Xs.shape)
  
    Ys = self.f(Xs)
    Ys = Ys + np.random.normal(0, self.sigma_Y, Ys.shape)

    return Xs, Ys, Zs

  def generate_joint_samples(self, n):
    """Generates n independent samples of (X, Y, Z)."""
    
    Zs = np.random.uniform(low=0., high=1., size=[n, self.d_Z])
    
    Xs = self.g(Zs)
    Xs = Xs + np.random.normal(0, self.sigma_X, Xs.shape)
  
    Ys = self.f(Xs)
    Ys = Ys + np.random.normal(0, self.sigma_Y, Ys.shape)
  
    return Xs, Ys, Zs
